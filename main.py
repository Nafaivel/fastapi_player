from fastapi import FastAPI
from pydantic import BaseModel

import subprocess

app = FastAPI()


class Url(BaseModel):
    url: str | None = None

@app.put("/open/video")
def spawn_player(*, url: Url):
    try:
        subprocess.run(f"mpv '{url.url}'", shell=True, check=True)
        return {"message": "spawned"}
    except:
        return {"message": "something went wrong"}

